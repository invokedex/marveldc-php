<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

/*Route::resource('movies', 'MoviesController');*/

Route::get('/actors','ActorController@index');
Route::get('/oneActor/{actor}', 'ActorController@showActor')->name('one.actor');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//CRUD routes
Route::resource('comments', 'CommentController');


Route::get('/movies','MoviesController@list')
    ->name('movie.List');

Route::get('/movieOne/{movie}','MoviesController@showById')
    ->name('movies.show');

Route::get('/movieSearch/{search}', 'MoviesController@search')
    ->name('movies.search');
Route::get('/genre','GenreController@index');
Route::get('/years','YearsController@index');
