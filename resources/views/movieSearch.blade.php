@extends('layouts.main')

@section('content')
    <br>
    <br>
    <br>
    <div class="page-single">
        <div class="container">
            <div class="row ipad-width2">
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="ceb-item-style-2">
                                <div class="movie-item">
                                    <div class="mv-img">
                                        <img src={{url($movieSearch->url)}} alt="" width="285" height="437">
                                    </div>
                                    <div class="title-in">
                                        <h6><a href="{{route('movies.show', $movieSearch->id)}}">{{$movieSearch->FilmName}}</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



