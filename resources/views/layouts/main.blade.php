<!DOCTYPE html>
<html lang="en" class="no-js">

    <head>
        <!-- Basic need -->
        <title>Open Pediatrics</title>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <link rel="profile" href="#">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{$title ?? ''}} @if($title ?? false)~@endif {{ config('app.name', 'MarvelDC') }}</title>

        <!--Google Font-->
        <link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Dosis:400,700,500|Nunito:300,400,600' />
        <!-- Mobile specific meta -->
        <meta name=viewport content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone-no">

        <!-- CSS files -->
        <link rel="stylesheet" href="{{url('../css/plugins.css')}}">
        <link rel="stylesheet" href="{{url('../css/style.css')}}">
    </head>
    <body>

    <header class="ht-header">
        <div class="container">
            <nav class="navbar navbar-default navbar-custom">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header logo">
                    <div class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <div id="nav-icon1">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <a href="{{url('/resources/views/welcome.blade.php')}}}"><img class="logo" src="{{url('/images/logo1.jpg')}}" alt="" width="119" height="58"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse flex-parent" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav flex-child-menu menu-left">

                        <a href="#"  class="btn btn-default dropdown-toggle lv1" data-toggle="dropdown">
                            Home
                        </a>

                        <li class="dropdown first">
                            <a class="btn btn-default dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
                                Genres
                            </a>
                            <ul class="dropdown-menu level1">
                                <li><a href="genre">Жанры</a></li>
                            </ul>
                        </li>
                        <li class="dropdown first">
                            <a class="btn btn-default dropdown-toggle lv1" href="#" data-toggle="dropdown" data-hover="dropdown">
                                Actors
                            </a>
                            <ul class="dropdown-menu level1">
                                <li><a href="actors">Актеры</a></li>
                            </ul>

                        </li>
                        <li class="dropdown first">
                            <a class="btn btn-default dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
                                Years
                            </a>
                            <ul class="dropdown-menu level1">
                                <li><a href="years">Года</a></li>
                            </ul>
                        </li>
                    </ul>

                    @if(!Auth::check())
                        <ul class="nav navbar-nav flex-child-menu menu-right">
                            <a href="login">Login</a> &nbsp;  &nbsp;  &nbsp;
                            <a href="register">Register</a>
                        </ul>
                    @else()
                        <ul class="nav navbar-nav flex-child-menu menu-right">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}<span class="caret"></span>
                            </a>
                        </ul>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    @endif
                </div>
            </nav>
        </div>

        <form method="get" action="{{route('movies.search', $nameSearch ?? '')}}">
            <div class="top-search">
                <input type="text" placeholder="Search for a DC or Marvel movie" name="search">
                <input type="submit"/>
            </div>
        </form>
    </header>

        @yield('content')

        <footer class="ht-footer">
            <div class="container">
                <div class="col-md-2">
                    <a href="/"><img class="logo" src="{{url('/images/logo1.jpg')}}" alt=""></a>
                </div>
                <div class="col-md-2">
                    <p>Кабанбай батыра, 10<br>
                        Астана, Казахстан</p>
                </div>
                <div class="col-md-3">
                    <h4>Resources</h4>
                    <ul>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>Account</h4>
                    <ul>
                        <li><a href="#">My Account</a></li>
                    </ul>
                </div>
            </div>
            <div class="ft-copyright">
                <div class="backtotop">
                    <p><a href="#" id="back-to-top">Back to top  <i class="ion-ios-arrow-thin-up"></i></a></p>
                </div>
            </div>
        </footer>
<!-- end of footer section-->

<script src="{{ url('/js/jquery.js')}}"></script>
<script src="{{ url('/js/plugins.js')}}"></script>
<script src="{{ url('/js/plugins2.js')}}"></script>
<script src="{{ url('/js/custom.js')}}"></script>
</body>


</html>
