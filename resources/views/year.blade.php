@extends('layouts.main')


@section('content')
    <br>
    <br>
    <div class="page-single">
        <div class="container">
            <div class="row ipad-width2">
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-4">
                            @foreach($years as $year)
                                <div class="ceb-item-style-2">
                                    <h2>{{$year->dateFilm}}</h2>
                                </div>

                        @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
@endsection
