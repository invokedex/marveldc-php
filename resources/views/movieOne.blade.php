@extends('layouts.main')

@section('content')
    <br>
    <br>
    <br>
    <br>
    <div class="row ipad-width2">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="movie-img sticky-sb">
                <img src={{url($movieOne->url)}} alt="">

            </div>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="movie-single-ct main-content">
                <h1 class="bd-hd">{{$movieOne->FilmName}}<span>2015</span></h1>
                <div class="movie-tabs">
                    <div class="tabs">
                        <ul class="tab-links tabs-mv">
                            <li class="active"><a href="#overview">Overview</a></li>
                            <li><a href="#reviews"> Reviews</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="overview" class="tab active">
                                <div class="row">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <!-- movie cast -->
                                        <div class="mvcast-item">
                                            <div class="cast-it">
                                                @foreach($actorsByMovie as $actors)
                                                    <div class="cast-left">
                                                        <div class="col-md-3">
                                                            <div class="row-cols-md-12">
                                                                <img src={{url($actors->url)}} alt="">
                                                            </div>
                                                        </div>
                                                        <a href="#">{{$actors->FIO}}</a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <div class="sb-it">
                                            <h6>Stars: </h6>
                                            @foreach($actorsByMovie as $actors)
                                                <p><a href="#">{{$actors->FIO}}</a>
                                            @endforeach
                                        </div>
                                        <div class="sb-it">
                                            <h6>Genres:</h6>
                                            @foreach($genresByMovie as $genres)
                                                <p><a href="#">{{$genres->genreName}}</a>
                                            @endforeach
                                        </div>
                                        <div class="sb-it">
                                            <h6>Release Date:</h6>
                                            @foreach($yearsByMovie as $years)
                                                <p>{{$years->dateFilm}}</p>
                                            @endforeach
                                        </div>
                                        <div class="ads">
                                            <img src="images/uploads/ads1.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="reviews" class="tab review">
                                <div class="row">
                                    <div class="rv-hd">
                                        <div class="div">
                                            <h2>{{$movieOne->FilmName}}</h2>
                                        </div>
                                        @if(Auth::check())
                                            <a href="{{route('comments.create', $movieOne->id)}}" class="redbtn">Write Comment</a>
                                        @endif
                                    </div>
                                    @forelse($comments as $comment)
                                        <a href="{{route('comments.show', $comment)}}" class="mb-3 card card-body">
                                            <h3 class="mb-0">
                                                {{$comment ?? ''}}
                                            </h3>
                                        </a>
                                    @empty
                                        <div class="alert alert-secondary">Комментариев пока что нету</div>
                                    @endforelse

                                    @if(auth()->check())
                                        <div class="mb-3 btn-group btn-group-sm">
                                            @if(auth()->user()->can('update',$comment ?? ''))
                                                <a class="btn btn-info" href="{{route('comments.edit', $comment ?? '')}}">Изменить</a>
                                            @endif
                                            @if(auth()->user()->can('delete',$comment ?? ''))
                                                <a class="btn btn-danger delete-link" href="{{route('comments.destroy', $comment ?? '')}}" data-target="delete-form">Удалить</a>
                                                <form id="delete-form" action="{{route('comments.destroy', $comment ?? '')}}" method="POST" class="d-none">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            @endif
                                        </div>
                                    @endif
                                    <h1>{{$comment ?? ''}}</h1>
                                    <p class="lead">{{$comment ?? ''}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    @if(auth()->user()->can('delete', $comment ?? ''))
        <script>
            let link=document.querySelector('.delete-link');
            let id=link.dataset.target;
            link.addEventListener('click', function (event) {
                event.preventDefault();
                document.getElementById(id).submit();
            });
        </script>
    @endif
    -->
    <br>
    <br>
    <br>
    <br>
@endsection

