@extends('layouts.main')

@section('content')

    <div id="preloader">
        <img class="logo" src="{{url('../images/logo1.jpg')}}" alt="" width="119" height="58">
        <div id="status">
            <span></span>
            <span></span>
        </div>
    </div>


    <div class="slider movie-items">
        <div class="container">
            <div class="row">
                <div  class="slick-multiItemSlider">
                    @foreach($movies as $movie)
                    <div class="movie-item" id="myFilms">
                            <div class="mv-img">
                                <img src={{url($movie->url)}} alt="" width="285" height="437">
                            </div>
                            <div class="title-in">
                                <h6><a href="{{route('movies.show', $movie->id)}}">{{$movie->FilmName}}</a></h6>
                            </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


    <div class="trailers">
        <div class="container">
            <div class="row ipad-width">
                <div class="col-md-12">
                    <div class="title-hd">
                        <h2>Best trailers</h2>
                    </div>
                    <div class="videos">
                        <div class="slider-for-2 video-ft">
                            <div>
                                <iframe class="item-video" src="#" data-src="https://www.youtube.com/embed/1Q8fG0TtVAY"></iframe>
                            </div>
                            <div>
                                <iframe class="item-video" src="#" data-src="https://www.youtube.com/embed/w0qQkSuWOS8"></iframe>
                            </div>
                            <div>
                                <iframe class="item-video" src="#" data-src="https://www.youtube.com/embed/44LdLqgOpjo"></iframe>
                            </div>
                            <div>
                                <iframe class="item-video" src="#" data-src="https://www.youtube.com/embed/gbug3zTm3Ws"></iframe>
                            </div>
                            <div>
                                <iframe class="item-video" src="#" data-src="https://www.youtube.com/embed/e3Nl_TCQXuw"></iframe>
                            </div>
                            <div>
                                <iframe class="item-video" src="#" data-src="https://www.youtube.com/embed/NxhEZG0k9_w"></iframe>
                            </div>
                        </div>
                        <div class="slider-nav-2 thumb-ft">
                            <div class="item">
                                <div class="trailer-img">
                                    <img src="{{url('/images/uploads/trailer7.jpg')}}" alt="photo by Barn Images" width="4096" height="2737">
                                </div>
                                <div class="trailer-infor">
                                    <h4 class="desc">Wonder Woman</h4>
                                    <p>2:30</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trailer-img">
                                    <img src="{{url('/images/uploads/trailer2.jpg')}}" alt="photo by Barn Images" width="350" height="200">
                                </div>
                                <div class="trailer-infor">
                                    <h4 class="desc">Oblivion: Official Teaser Trailer</h4>
                                    <p>2:37</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trailer-img">
                                    <img src="{{url('/images/uploads/trailer6.jpg')}}" alt="photo by Joshua Earle">
                                </div>
                                <div class="trailer-infor">
                                    <h4 class="desc">Exclusive Interview:  Skull Island</h4>
                                    <p>2:44</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trailer-img">
                                    <img src="{{url('/images/uploads/trailer3.png')}}" alt="photo by Alexander Dimitrov" width="100" height="56">
                                </div>
                                <div class="trailer-infor">
                                    <h4 class="desc">Logan: Director James Mangold Interview</h4>
                                    <p>2:43</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trailer-img">
                                    <img src="{{url('/images/uploads/trailer4.png')}}" alt="photo by Wojciech Szaturski" width="100" height="56">
                                </div>
                                <div class="trailer-infor">
                                    <h4 class="desc">Beauty and the Beast: Official Teaser Trailer 2</h4>
                                    <p>2: 32</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="trailer-img">
                                    <img src="{{url('/images/uploads/trailer5.jpg')}}" alt="photo by Wojciech Szaturski" width="360" height="189">
                                </div>
                                <div class="trailer-infor">
                                    <h4 class="desc">Fast&Furious 8</h4>
                                    <p>3:11</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $("#search").keyup(function(){
                _this = this;
                $.each($("#myFilms tbody tr"), function() {
                    if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
            });
        });
    </script>
@endsection
