@extends('layouts.main')


@section('content')
    <br>
    <br>
<div class="page-single">
    <div class="container">
        <div class="row ipad-width2">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-4">
                        @foreach($actor as $act)
                        <div class="ceb-item-style-2">
                            <img src={{url($act->url)}} alt="">
                            <div class="ceb-infor">
                                <h2><a href="{{route('one.actor', $act->id)}}">{{$act->FIO}}</a></h2>
                                <span>actor, uk</span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="filter-sidebar" action="{{route('one.actor', $act->id)}}" method="get">
    <div class="filter-select-wrap">
        <input id="filter-address" class="filter-select filter-tax" name="location" data-address='{{$act->FIO}}' type="text" placeholder="Actor location"/>
        <ul class="ul-addresses"></ul>
    </div>
</form>
    <br>
    <br>
    <br>
    <br>

<script>
    //функция поиска совпадений вводимых символов
    function findEl(el, array, value) {
        var coincidence = false;
        el.empty();//очищаем список совпадений
        for (var i = 0; i < array.length; i++){
            if (array[i].match('^'+value)){//проверяем каждый елемент на совпадение побуквенно
                el.children('li').each(function (){//проверяем есть ли совпавшие елементы среди выведенных
                    if(array[i] === $(this).text()) {
                        coincidence = true;//если есть совпадения то true
                    }
                });
                if(coincidence === false){
                    el.append('<li class="js-filter-address">'+array[i]+'</li>');//если нету совпадений то добавляем уникальное название в список
                }
            }
        }
    }

    var filterInput = $('#filter-address'),
        filterUl = $('.ul-addresses');
    //проверка при каждом вводе символа
    filterInput.bind('input propertychange', function(){
        if($(this).val() !== ''){
            filterUl.fadeIn(100);
            findEl(filterUl,$(this).data('address'),$(this).val());
        }
        else{
            filterUl.fadeOut(100);
        }
    });
    //при клике на елемент выпадалки присваиваем значение в инпут и ставим триггер на его изменение
    filterUl.on('click','.js-filter-address', function(e){
        $('#filter-address').val('');
        filterInput.val($(this).text());
        filterInput.trigger('change');
        filterUl.fadeOut(100);
    });
</script>
@endsection
