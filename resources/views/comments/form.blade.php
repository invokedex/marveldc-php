<?php
$update=isset($comment);
?>
@extends('layouts.main')

@section('content')
    <br>
    <br>
    <br>
    <br>
<h1>{{$title}}</h1>
    <form action="{{route('comments.store', $movie->id)}}" method="POST" class="card card-body">
        @csrf
        @if($update)
            @method('PUT')
            @endif
           <div class="form-group">
               <label for="title">Название<span class="text-danger">*</span></label>
               <input type="text"
                      name="title"
                      id="title"
                      class="form-control @error('title') is-invalid @enderror"
                      placeholder="Введите заголовок..."
                      value="{{old('title') ?? ($comment->title ?? '')}}">
               @error('title')
               <div class="invalid-feedback">
                   {{$message}}
               </div>
               @enderror
           </div>

            <div class="form-group">
                <label for="content">Комментарий<span class="text-danger">*</span></label>
                <textarea name="content"
                          id="content"
                          class="form-control @error('content') is-invalid @enderror"
                          placeholder="Напишите отзыв о фильме">{{old('content') ?? ($comment->content ?? '')}}</textarea>
                @error('content')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
        <div>
        <button class="btn-success">{{$update ? 'Обновить комментарии' : 'Написать комментарии'}}</button>
        </div>
    </form>
    <br>
    <br>
    <br>
    <br>
@endsection
