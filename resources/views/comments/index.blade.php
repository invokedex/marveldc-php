@extends('layouts.main')

@section('content')
<br>
<br>
<br>
<br>
    <div class="d-flex align-content-between align-items-center">
        <h1>Комментарии</h1>
        @if(Auth::check())
        <a href="{{route('comments.create')}}" class="btn btn-success ml-auto">
            Написать коментарии
        </a>
        @endif
    </div>
    @forelse($comments as $comment)
        <a href="{{route('comments.show', $comment)}}" class="mb-3 card card-body">
            <h3 class="mb-0">
            {{$comment->content}}
            </h3>
        </a>
        @empty
        <div class="alert alert-secondary">Комментариев пока что нету</div>
    @endforelse

    <br>
    <br>
    <br>
    <br>
    <br>
@endsection
