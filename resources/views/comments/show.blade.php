@extends('layouts.main')

@section('content')
    <br>
    <br>
    <br>
    <br>
    <br>
    @if(auth()->check())
        <div class="mb-3 btn-group btn-group-sm">
            @if(auth()->user()->can('update',$comment))
            <a class="btn btn-info" href="{{route('comments.edit', $comment)}}">Изменить</a>
                @endif
                @if(auth()->user()->can('delete',$comment))
                    <a class="btn btn-danger delete-link" href="{{route('comments.destroy', $comment)}}" data-target="delete-form">Удалить</a>
                    <form id="delete-form" action="{{route('comments.destroy', $comment)}}" method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endif
        </div>
        @endif


    <h1>{{$comment->title}}</h1>
    <p class="lead">{{$comment->content}}</p>


@if(auth()->user()->can('delete', $comment))
    <script>
        let link=document.querySelector('.delete-link');
        let id=link.dataset.target;
        link.addEventListener('click', function (event) {
            event.preventDefault();
            document.getElementById(id).submit();
        });
    </script>
    @endif

    <br>
    <br>
    <br>
    <br>
    @endsection
