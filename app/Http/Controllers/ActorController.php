<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use Illuminate\Http\Request;

class ActorController extends Controller
{
    function index()
    {
        return view('actors', [
            'actor' => Actor::all()
        ]);
    }

    function showActor(Actor $actor)
    {
        return view('oneActor', [
            'actor' => $actor
        ]);
    }

    function search(Actor $actor)
    {
        $act = DB::table('movies')->where('id', $actor->id)->first();
        return view('welcome',[
            'actorSearch' => $act
        ]);
    }
}//movies
//movieOne/1
//marvaldc.nn
//actors
//actorOne/1
