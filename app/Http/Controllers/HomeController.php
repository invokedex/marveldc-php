<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;

class HomeController extends Controller
{
//   public function __construct()
//   {
//       $this->middleware('auth');
//   }

    function index()
    {
        return view('welcome',[
            'movies' => Movie::all()
        ]);
    }
}
