<?php

namespace App\Http\Controllers;

use App\Year;
use Illuminate\Http\Request;

class YearsController extends Controller
{
    function index()
    {
        return view('year',[
            'years' => Year::all()
        ]);
    }
}
