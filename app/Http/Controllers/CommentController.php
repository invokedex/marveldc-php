<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Movie;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    public function index()
    {
        return view('comments.index' ,[
        'title' => 'Комментарии',
        'comments' => Comment::all()
        ]);
    }
    public function create(Movie $movie)
    {
        return view('comments.form',[
            'title' => 'Новый комментарий',
            'movie' => $movie
        ]);
    }
    protected function rules(){
        return[
          'title'=>'required',
          'content'=>'required'
        ];
    }
    public function store(Request $request, Movie $movie)
    {
        $request->validate($this->rules());


        $data = $request->except('_token');
        $comment=new Comment($data);
        $comment->movie_id = $movie;
        $comment->user_id=auth()->user()->id;
        $comment->save();

        return redirect()->route('movieOne', $comment->movie_id);
    }

    public function show(Comment $comment)
    {
        return view('movieOne',[
            'title' => $comment->title,
            'comment' => $comment
        ]);
    }

    public function edit(Comment $comment)
    {
        $this->authorize('update', $comment);
        return view('comments.form',[
           'title'=>'Изменить' . $comment->title,
           'comment' => $comment
        ]);
    }

    public function update(Request $request, Comment $comment)
    {
        $this->authorize('update', $comment);
        $request->validate($this->rules());
        $data=$request->except(['_token', '_method']);
        $comment->fill($data);
        $comment->save();
        return redirect()->route('movieOne', $comment);
    }

    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return redirect()->route('comments.index');
    }
}
