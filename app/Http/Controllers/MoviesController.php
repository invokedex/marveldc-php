<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MoviesController extends Controller
{
    function list()
    {
        return view('movie',[
            'movies' => Movie::all()
        ]);
    }

    function showById(Movie $movie)
    {
        $comments = Comment::all();
        $connectionMovie = DB::table('connectionid')->select()->where('moviesId', '=', $movie->id)->first();
        $searchGenre = DB::table('genre')->where('id', '=', $connectionMovie->genreId)->get();
        $searchActors = DB::table('actors')->where('id', '=', $connectionMovie->actorsId)->get();
        $searchYear = DB::table('years')->where('id', '=', $connectionMovie->yearId)->get();
        return view('movieOne', [
            'movieOne' => $movie,
            'yearsByMovie' => $searchYear,
            'genresByMovie' => $searchGenre,
            'actorsByMovie' => $searchActors,
            'comments' => $comments
        ]);
    }

    function search($search)
    {
        $film = Movie::findOrFail($search);
        return view('movieSearch', [
            'movieSearch' => $film
        ]);
    }

}
