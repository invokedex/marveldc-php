<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreController extends Controller
{
    function index()
    {
        return view('genre',[
            'genres' => Genre::all()
        ]);
    }


    function showById(Genre $genre)
    {
        $connectionGenre = DB::table('connectionId')->select()->where('genreId', '=', $genre->id)->first();
        $searchMovie = DB::table('movies')->where('id', '=', $connectionGenre->moviesId)->get();
        $searchYear = DB::table('years')->where('id', '=', $connectionGenre->yearId)->get();
        return view('genre', [
            'moviesByGenre' => $searchMovie,
            'yearsByGenre' => $searchYear
        ]);
    }


    function search(Genre $genre)
    {
        $zhanr = DB::table('genre')->where('genreName', '=', $genre->genreName)->first();
        return view('welcome',[
            'movieSearch' => $zhanr
        ]);
    }
}
