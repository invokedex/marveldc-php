<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=[
        'title','content','user_id'
    ];

//    function getUserAttribute(){
//        return User::find($this->user_id);
//    }
}
