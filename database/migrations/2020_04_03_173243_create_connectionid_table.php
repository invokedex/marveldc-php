<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnectionidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connectionid', function (Blueprint $table) {
            $table->id();
            $table->foreignId('moviesId')
                ->references('id')
                ->on('movies')
                ->onDelete('cascade');
            $table->foreignId('actorsId')
                ->references('id')
                ->on('actors')
                ->onDelete('cascade');
            $table->foreignId('genreId')
                ->references('id')
                ->on('genre')
                ->onDelete('cascade');
            $table->foreignId('yearId')
                ->references('id')
                ->on('years')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connectionid');
    }
}
